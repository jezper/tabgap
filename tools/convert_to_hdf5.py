import tabgap.read
import tabgap.write
import argparse

default = argparse.ArgumentDefaultsHelpFormatter
parser = argparse.ArgumentParser(formatter_class=default)
parser.add_argument('tabgap', help='.tabgap file in raw text to convert to .tabgap.h5')
args = parser.parse_args()

tg = tabgap.read.read_tabgap_potfile(args.tabgap)
outfile = f'{args.tabgap.split("/")[-1]}.h5'
tabgap.write.write_tabgap_potfile_h5(outfile, **tg)
print(outfile)
