from tabgap.tabulate import tabGAP
import pytest


@pytest.fixture
def tg_2b():
    return tabGAP('../examples/test_files/gap_W-Ta_example_2b.xml',
                  n2b=100)

@pytest.fixture
def tg_2b3b():
    return tabGAP('../examples/test_files/gap_W-Ta_example_2b+3b.xml',
                  n2b=100, n3b=(4, 4, 4))

@pytest.fixture
def tg_2b3beam():
    return tabGAP('../examples/test_files/gap_W-Ta_example_2b+3b+eam.xml',
                  n2b=100, n3b=(4, 4, 4), neam=100, rho_max=8)

@pytest.fixture
def tg_2beam():
    return tabGAP('../examples/test_files/gap_W-Ta_example_2b+eam.xml',
                  n2b=100, neam=100, rho_max=8)

@pytest.fixture
def tg_2bneam():
    return tabGAP('../examples/test_files/gap_W-Ta_example_2b+n_eam.xml',
                  n2b=100, neam=100, rho_max=8)
