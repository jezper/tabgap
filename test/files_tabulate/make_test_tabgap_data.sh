n2b=100
n3b=4
neam=100
rho_max=8

python3 ../../tabgap/tabulate.py ../../examples/test_files/gap_W-Ta_example_2b.xml -n2b $n2b
mv out-tab/* 2b/
mv Ta-W.tabgap 2b/

python3 ../../tabgap/tabulate.py ../../examples/test_files/gap_W-Ta_example_2b+eam.xml -n2b $n2b -neam $neam -rhomax $rho_max
mv out-tab/* 2beam/
mv Ta-W.eam.fs 2beam/

python3 ../../tabgap/tabulate.py ../../examples/test_files/gap_W-Ta_example_2b+3b+eam.xml -n2b $n2b -neam $neam -rhomax $rho_max -n3b $n3b $n3b $n3b
mv out-tab/* 2b3beam/
mv Ta-W.* 2b3beam/

python3 ../../tabgap/tabulate.py ../../examples/test_files/gap_W-Ta_example_2b+3b.xml -n2b $n2b -n3b $n3b $n3b $n3b
mv out-tab/* 2b3b/
mv Ta-W.tabgap 2b3b/

python3 ../../tabgap/tabulate.py ../../examples/test_files/gap_W-Ta_example_2b+n_eam.xml -n2b $n2b -neam $neam -rhomax $rho_max
mv out-tab/* 2bneam/
mv Ta-W*.eam.fs 2bneam/

rm -r out-tab
