Use as:

```
mass 1 26.981539
pair_style hybrid/overlay eam/fs tabgap
pair_coeff * * eam/fs /path/to/Al.eam.fs Al
pair_coeff * * tabgap /path/to/Al.tabgap Al no yes
```
