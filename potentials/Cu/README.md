Use as:

```
mass 1 63.546
pair_style hybrid/overlay eam/fs tabgap
pair_coeff * * eam/fs /path/to/Cu.eam.fs Cu
pair_coeff * * tabgap /path/to/Cu.tabgap Cu no yes
```
