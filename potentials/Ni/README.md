Use as:

```
mass 1 58.6934
pair_style hybrid/overlay eam/fs tabgap
pair_coeff * * eam/fs /path/to/Ni.eam.fs Ni
pair_coeff * * tabgap /path/to/Ni.tabgap Ni no yes
```
