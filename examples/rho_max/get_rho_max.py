from tabgap.utils import get_rho_max_from_xyz
from tabgap.quip_tools import GAP

# Get rho_max from xyz file (maximum total density from eam_density descriptors),
# in order to estimate a large enough rho_max for tabulation.
# The default rho_max=10 in tabGAP is often high enough, but not always...,
# (for example when using 'polycutoff' or 'coscutoff' pair_function with rmin > 0).
# A good xyz file would be the most dense structures from your training data.
# (avoid using large systems or an entire big training xyz, because it will be slow...)

gap_xml = '../test_files/gap_W-Ta_example_2b+n_eam.xml'
xyz = '../test_files/db_W-Ta_example_no_isolated.xyz'

gap = GAP(gap_xml)
rho_max = get_rho_max_from_xyz(xyz, gap)

print(f'rho_max {rho_max} in {xyz}')
print(f'use e.g. 1.5*rho_max ~ {int(1.5*rho_max)} when making tabGAP potential files')
