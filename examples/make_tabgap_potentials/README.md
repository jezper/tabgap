Example scripts of how to create tabGAP potential files for different combinations of descriptors, by tabulating GAP --> tabGAP energy grids for each descriptor. The energy grid data will be saved in a folder `out-tab` and can be inspected and visualised afterwards.

All these examples could also be done by directly running tabulate.py. For example, for the 2b+3b+eam example:

    python ../../tabgap/tabulate.py ../test_files/gap_W-Ta_example_2b+3b+eam.xml -n2b 2000 -neam 2000 -n3b 60 60 60 -nc 3

Note: the 3b grids will take several minutes to compute per element triplet (with a two-element potential here, there are 6 triplets). Use as many cores as you can afford to speed it up (`-nc`).
