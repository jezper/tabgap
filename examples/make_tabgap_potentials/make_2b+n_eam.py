from tabgap.tabulate import tabGAP

gap_xml = '../test_files/gap_W-Ta_example_2b+n_eam.xml'

verbose = True
tg = tabGAP(gap_xml, n2b=2000, neam=2000, verbose=verbose)
tg.compute_energies(ncores=2, verbose=verbose)
tg.write_potential_files(verbose=verbose)
