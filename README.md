# tabGAP - tabulated Gaussian approximation potentials

Tools to make and run tabulated (faster) versions of GAP machine-learning interatomic potentials trained with low-dimensional descriptors (2-body, 3-body, EAM density). See more details in Refs. [1, 2].

## How to:
###  Create a tabulated GAP potential file

 1. Train a GAP using any combination of 2-body, 3-body and EAM density descriptors using [QUIP](https://github.com/libAtoms/QUIP) (NOTE: for the EAM-density descriptor, you need the GAP source from https://github.com/jesperbygg/GAP branch `eam_density_multi`).
    <details>
    <summary>Tips and tricks (click me!):</summary>
    
    - For the `eam_density` descriptor, `nsparse=20` is enough (same as for `distance_2b`). One can try the different "modes" (default is `blind`, see details in [1]), they usually give similar accuracy but possibly different extrapolation behaviour. Same for the different choices of `pair_function`.
    - For the 3-body `angle_3b` descriptor, a relatively short cutoff (3.5-4.5 Å) is usually sufficient to get a good balance between speed and accuracy.
    </details>


 2. Make tabulated potential files. Run [tabgap/tabulate.py](tabgap/tabulate.py) directly (see `--help` for details), or write something like:
    ```
    from tabgap.tabulate import tabGAP

    tg = tabGAP('/path/to/gapfile.xml',
                    n2b=1000, n3b=(80, 80, 80), neam=1000)
    tg.compute_energies(ncores=3)
    tg.write_potential_files()
    ```

    <details>
    <summary>Tips and tricks (click me!):</summary>

     - The tabulation uses [QUIP](https://github.com/libAtoms/QUIP) to evaluate pair, triplet, and EAM energies. It must be installed and the `quip` executable found by the environment variable `QUIP_COMMAND`. For this, simply do `export QUIP_COMMAND=/path/to/quip` (or put it in your `~/.bashrc`). Otherwise the code will tell you to do so.
     - Computing the 3b grid will take several minutes per element triplet. Increase `ncores` for faster tabulation (will perform trivial parallelisation by splitting the triplets into `ncores` chunks and computing them on separate CPU cores). 
     - Use large enough 3b grid to reduce interpolation errors. For a cutoff distance of ~4 Å, a `n3b=(80, 80, 80)` grid should be sufficient for negligible interpolation errors (see also the [example](examples/converge_interpolation_error/get_3b_convergence.py)).
     - If you use non-default values for the `pair_function`, `mode`, or `prefactor` in the `eam_density` descriptor during training of the GAP, you may have to increase `rho_max` in the `tabGAP` class (default is 10). You can use [visualise.py](tabgap/visualise.py) script to plot the tabulated EAM (embedding) energy and see that it levels out at 0 (which is physically not correct, sometime one may have to manually modify the machine-learned embedding energy to monotonically increase at high densities to achieve the correct coordination dependence).
     - See also the [examples](examples).
     </details>

###  Run a [LAMMPS](https://lammps.org) simulation

 - Requires lammps version 24 March 2022 or newer!

 - Copy the files `pair_tabgap.*` from [lammps](lammps) into your `lammps/src/` folder and compile normally.
   - For [KOKKOS](https://docs.lammps.org/Speed_kokkos.html) support (e.g. GPU), also copy the files from [lammps/KOKKOS](lammps/KOKKOS) into your `lammps/src/KOKKOS` and compile following the lammps documentation.
   - For using HDF5 file format for the .tabgap potential file (.tabgap.h5), copy the files from [lammps/hdf5](lammps/hdf5) into your `lammps/src/`. This is recommended when running many short runs with large (many-element) tabGAP potentials. A [script](tools/convert_to_hdf5.py) is provided for converting from raw-text .tabgap files to .tabgap.h5.

 - Use the `pair_style tabgap` as for example:
    ```
    pair_style   tabgap
    pair_coeff   * * W.tabgap W yes yes
    ```
    for a 2b+3b W potential. Or, more commonly:
    ```
    pair_style   hybrid/overlay tabgap eam/fs
    pair_coeff   * * tabgap W.tabgap W no yes
    pair_coeff   * * eam/fs W.eam.fs W
    ```
    for a 2b+3b+EAM potential.

## References

If you use this code to run or create a tabGAP potential, please cite:

[1] J. Byggmästar, K. Nordlund, and F. Djurabekova, *Simple machine-learned interatomic potentials for complex alloys*, Phys. Rev. Materials **6**, 083801 (2022), https://doi.org/10.1103/PhysRevMaterials.6.083801, https://arxiv.org/abs/2203.08458

[2] J. Byggmästar, K. Nordlund, F. Djurabekova, *Modeling refractory high-entropy alloys with efficient machine-learned interatomic potentials: Defects and segregation*, Phys. Rev. B **104**, 104101 (2021), https://doi.org/10.1103/PhysRevB.104.104101, https://arxiv.org/abs/2106.03369

## Potentials

Potential files for some tabGAPs are provided in the [potentials](potentials) folder. If you use any of the potentials, please cite the corresponding reference.

## Contributors

- Jesper Byggmästar (main)
- Ville-Markus Yli-Suutala (lammps kokkos)
