import numpy as np
from tabgap.periodic_table import *
import h5py


def _finalise_tabgap_grid_parameters(interactions, grid_params):
    """ helper function to check and fill in distance and grid size pair/triplet dictionaries
    interactions: list of pairs/triplets that should exist (just pass energies_2b/3b.keys())
    grid_params: r2b, r3b, n2b or n3b.
        - if only one set of parameters given, set the same for all pairs/triplets (make dict)
        - if already a dictionary, check that all interactions are given.
    Return: dictionary with parameters for all interactions (if passed the check).
    """

    is_2b = False
    is_3b = False

    # check if pairs or triplets: only either one
    N_intrct = list(set([len(i) for i in interactions]))
    if len(N_intrct) == 0:
        # no interactions
        return grid_params
    elif len(N_intrct) > 1:
        print(interactions)
        raise TypeError(f'bad list of interactions, should be pairs only or triplets only!')
    else:
        if N_intrct[0] == 2:
            is_2b = True
        elif N_intrct[0] == 3:
            is_3b = True
        else:
            print(interactions)
            raise TypeError(f'bad list of interactions, should be pairs or triplets only!')

    if isinstance(grid_params, dict):
        # strict check that all interactions are given. Brute-force loop to catch missing ones
        for p in interactions:
            if p not in interactions:
                # check if symmetric case exists
                if is_2b:
                    psym = (p[1], p[0])
                elif is_3b:
                    psym = (p[0], p[2], p[1])
                if psym not in interactions:
                    raise KeyError(f'missing grid parameters for {p} in {grid_params}')
        return grid_params
    else:
        # only one set of params given --> user wants same for all interactions --> set up dict
        return {p: grid_params for p in interactions}


def write_tabgap_potfile_h5(filename, elements, e0, energies_2b, energies_3b, r2b, r3b, n2b, n3b,
                            line1='This line is a comment', line2='This line is also a comment'):
    """ Write potential file for lammps pair_style tabgap in HDF5 binary format.
    elements: list of elements
    e0: dict with isolated atom energies
    energies_2b: dict with pair energies (key is element tuple)
    energies_3b: dict with triplet energies (key is element triplet)
    r2b: range of 2b distances
    r3b: range or rij and rik in 3b grid
    n2b: 2b grid (number of 2b grid points)
    n3b: 3b grid (N, N, N)
    line1, line2: two strings, useful for saving some info (GAP label, DATE, etc.)

    NOTE: r2b, r3b, n2b, n3b can be given either as one set of parameters (then assuming same for
    all interactions) or as a dict with parameters for each pair/triplet (keys should match energies_xb)
    See _finalise_tabgap_grid_parameters() function.
    """

    if energies_2b is None:
        energies_2b = {}
    if energies_3b is None:
        energies_3b = {}

    r2b = _finalise_tabgap_grid_parameters(energies_2b.keys(), r2b)
    n2b = _finalise_tabgap_grid_parameters(energies_2b.keys(), n2b)
    r3b = _finalise_tabgap_grid_parameters(energies_3b.keys(), r3b)
    n3b = _finalise_tabgap_grid_parameters(energies_3b.keys(), n3b)

    with h5py.File(filename, 'w') as hdf:

        # allow two comments (e.g. date and gap label)
        dt = h5py.special_dtype(vlen=str)  # variable-length string data
        hdf.create_dataset('comment1', data=np.array(line1, dtype=dt))
        hdf.create_dataset('comment2', data=np.array(line2, dtype=dt))

        # isolated atom energies e0
        e0_dict = {element: e0[atomic_numbers[element]] for element in elements}
        group = hdf.create_group('e0')
        group.attrs['Nelements'] = len(elements)
        for key, value in e0_dict.items():
            group.attrs[key] = value

        # number of 2b and 3b pots
        hdf.create_dataset('npots', data=np.array((len(energies_2b.keys()), len(energies_3b.keys()))))

        # 2b
        for pair, data in energies_2b.items():
            group = hdf.create_group('-'.join(pair))

            # ele1 ele2
            group.attrs['element_i'] = pair[0]
            group.attrs['element_j'] = pair[1]
            # rmin, rcut
            group.create_dataset('grid_limits', data=np.array(r2b[pair]))
            # npoints
            group.attrs['N'] = n2b[pair]

            group.create_dataset('energies', data=np.asarray(data))

        # 3b
        for triplet, data in energies_3b.items():
            group = hdf.create_group('-'.join(triplet))

            # ele1 ele2 ele3
            group.attrs['element_i'] = triplet[0]
            group.attrs['element_j'] = triplet[1]
            group.attrs['element_k'] = triplet[2]
            # rij_min rik_min costheta_min rij_max rik_max costheta_max
            g = np.array((r3b[triplet][0], r3b[triplet][0], -1.0, r3b[triplet][1], r3b[triplet][1], 1.0))
            group.create_dataset('grid_limits', data=g)
            # nij nik ncostheta
            group.create_dataset('N', data=np.array(n3b[triplet]))

            group.create_dataset('energies', data=np.asarray(data))


def write_tabgap_potfile(filename, elements, e0, energies_2b, energies_3b, r2b, r3b, n2b, n3b,
                         line1='This line is a comment', line2='This line is also a comment'):
    """ Write potential file for lammps pair_style tabgap.
    elements: list of elements
    e0: dict with isolated atom energies
    energies_2b: dict with pair energies (key is element tuple)
    energies_3b: dict with triplet energies (key is element triplet)
    r2b: range of 2b distances
    r3b: range or rij and rik in 3b grid
    n2b: 2b grid (number of 2b grid points)
    n3b: 3b grid (N, N, N)
    line1, line2: two strings, useful for saving some info (GAP label, DATE, etc.)

    NOTE: r2b, r3b, n2b, n3b can be given either as one set of parameters (then assuming same for
    all interactions) or as a dict with parameters for each pair/triplet (keys should match energies_xb)
    See _finalise_tabgap_grid_parameters() function.

    .tabgap file format:
    First two line are comments, add some useful details there with line1, line2 arguments.
    3rd line is number of elements and energies of isolated atoms, e.g. 1 W -4.57.
    4th line is number of 2b pots and 3b pots, e.g. 3 6.
    5th line+ data (energies) with header for each potential (see below).
    """

    if energies_2b is None:
        energies_2b = {}
    if energies_3b is None:
        energies_3b = {}

    r2b = _finalise_tabgap_grid_parameters(energies_2b.keys(), r2b)
    n2b = _finalise_tabgap_grid_parameters(energies_2b.keys(), n2b)
    r3b = _finalise_tabgap_grid_parameters(energies_3b.keys(), r3b)
    n3b = _finalise_tabgap_grid_parameters(energies_3b.keys(), n3b)

    with open(filename, 'w') as pf:

        # 2 first lines are comments
        print(line1, file=pf)
        print(line2, file=pf)

        # 3rd line is isolated atom energies e0
        e0_line = f'{len(elements)}'
        for element in elements:
            e0_line += f' {element} {e0[atomic_numbers[element]]}'
        print(e0_line, file=pf)

        # 4th line is number of 2b and 3b pots
        print(f'{len(energies_2b.keys())} {len(energies_3b.keys())}', file=pf)

        # then comes the data, one pot at a time with header
        # 2b
        for pair, data in energies_2b.items():
            # ele1 ele2 rmin rcut npoints
            header = f'{pair[0]} {pair[1]} {r2b[pair][0]} {r2b[pair][1]} {n2b[pair]}'
            print(header, file=pf)
            for e in data:
                print(e, file=pf)

        # 3b
        for triplet, data in energies_3b.items():
            # ele1 ele2 ele3 rij_min rik_min costheta_min rij_max rik_max costheta_max nij nik ncostheta
            header = f'{triplet[0]} {triplet[1]} {triplet[2]} {r3b[triplet][0]} {r3b[triplet][0]}'
            header += f' -1.0 {r3b[triplet][1]} {r3b[triplet][1]} 1.0'
            header += f' {n3b[triplet][0]} {n3b[triplet][1]} {n3b[triplet][2]}'
            print(header, file=pf)
            for e in data:
                print(e, file=pf)


def write_eam_fs_potfile(filename, elements, r, pair_energies, pair_densities, embedding_energies,
                         d_r, d_density, rcut, lattice_constants, lattices, comment_lines=['comment'] * 3):
    """ https://lammps.sandia.gov/doc/pair_eam.html
    pair_style eam/fs
    See above link for format. Supports many elements.
    Pair potential interactions are symmetric ij = ji, but pair density contributions may not be, i.e. ij != ji  !!!
    Note that r, d_r, d_density, and rcut must be the same for all element pairs!
    elements should be list/array of elements, e.g. ['A', 'B'], assuming cross-interactions between all.
    pair_energies, pair_densities, embedding_energies, lattice_constants, and lattices should all be
    dictionaries with key as the element pair (e.g. 'AB') or element ('A'). All elements/pairs must exist.
    Pair energies, densities, embedding energies must all be provided in the same interval!!!
    """

    # some sanity-checks in input
    N_rho = len(list(embedding_energies.values())[0])
    for data in embedding_energies.values():
        assert(N_rho == len(data))
    N_r = len(r)
    for data in pair_energies.values():
        assert(N_r == len(data))
    for data in pair_densities.values():
        assert(N_r == len(data))

    with open(filename, 'w') as pf:

        print(comment_lines[0], file=pf)
        print(comment_lines[1], file=pf)
        print(comment_lines[2], file=pf)

        elements_line = f'{len(elements)}'
        for e in elements:
            elements_line += f'  {e}'
        print(elements_line, file=pf)

        print(f'{N_rho} {d_density} {N_r} {d_r} {rcut}', file=pf)

        #### First embedding and pair density functions for each element and element pair
        for i, e in enumerate(elements):
            # header
            Z = atomic_numbers[e]
            mass = atomic_masses[Z]
            print(f'{Z} {mass} {lattice_constants[e]} {lattices[e]}', file=pf)
            for F in embedding_energies[e]:
                print(F, file=pf)

            # loop over pairs in correct lammps order: 1e, 2e, ..., Ne, where e is current element
            for j, e2 in enumerate(elements):
                # TODO make sure this is correct, because lammps seems to read in them as e+e2 !???
                pair = e2 + e
                for d in pair_densities[pair]:
                    print(d, file=pf)

        #### Then pair potentials in correct order: (1, 1), (2, 1), (2, 2), (3, 1), (3, 2), (3, 3), ...
        # no header, and lammps wants data as r * pairpot
        for i, e in enumerate(elements):
            for j in range(0, i+1):
                # allow symmetry
                try:
                    data = pair_energies[(e, elements[j])]
                except KeyError:
                    data = pair_energies[(elements[j], e)]
                for k, energy in enumerate(data):
                    print(r[k] * energy, file=pf)


def write_datfile(grid, energies, filename):
    """ save file with energies for every grid point (r_pair, eam_pair_density or triplet) """

    # TODO make arrays and use numpy's write functions
    #       - can they append to file (need headers..) ?

    with open(filename, 'w') as pf:
        if len(np.shape(grid)) == 1:
            assert len(grid) == len(energies)
            for i, e in enumerate(energies):
                print(grid[i], e, file=pf)
        elif len(np.shape(grid)) == 4:
            nij, nik, nct = np.shape(grid)[0:-1]
            assert np.prod((nij, nik, nct)) == len(energies)
            c = 0
            for i in range(nij):
                for j in range(nik):
                    for k in range(nct):
                        print(grid[i,j,k,0], grid[i,j,k,1], grid[i,j,k,2], energies[c], file=pf)
                        c += 1
        else:
            raise Exception('unrecognized shape of grid')
