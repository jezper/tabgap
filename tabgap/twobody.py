import os
import numpy as np
from tabgap.quip_tools import *
from tabgap.spline_tools import filter_coeffs
from tabgap.write import *

# pair potential tabulation tools


def generate_grid_points(rmin, rcut, n):
    """ rij distances """
    return np.linspace(rmin, rcut, n)


def get_element_pairs(elements, symmetry=True):
    """ get all element pairs, with ij=ji symmetry or not """
    pairs = []
    for i, s1 in enumerate(elements):
        for j, s2 in enumerate(elements):
            if not symmetry:
                pairs.append((s1, s2))
            else:
                if j >= i:
                    pairs.append((s1, s2))
    return pairs


def make_grid_points_xyz(distances, dimer_symbols=('W', 'W'), n_xyzfiles=1, overlap_tolerance=1e-6):
    """ make .xyz file(s) with isolated dimers. Returns list of xyz files (even when only one!). """

    # make sure box_length > 2*rcut !
    box_length = 20
    if 2 * max(distances) > box_length:
        box_length = int(2 * max(distances) + 5)

    def write_pair_xyz(dists, xyzname):
        """ helper function to write xyz (much faster than calling ase.io.write) """
        with open(xyzname, 'w') as xyz:
            for r in dists:
                if r < overlap_tolerance:
                    r += overlap_tolerance
                positions=[(0, 0, 0), (r, 0, 0)]
                print(len(positions), file=xyz)
                line2 = f'Lattice="{box_length} 0.0 0.0 0.0 {box_length} 0.0 0.0 0.0 {box_length}"'
                line2 += f' Properties=species:S:1:pos:R:3 pbc="F F F"'
                print(line2, file=xyz)
                for ii, pos in enumerate(positions):
                    print(f'{dimer_symbols[ii]} {pos[0]} {pos[1]} {pos[2]}', file=xyz)

    # split in as many xyz files as requested
    if n_xyzfiles == 1:
        xyzname = f'pairs_{"-".join(dimer_symbols)}.xyz'
        write_pair_xyz(distances, xyzname)
        return [xyzname]
    else:
        xyz_out = []
        i_split = np.linspace(0, len(distances), n_xyzfiles, endpoint=False, dtype=int)
        for i in range(n_xyzfiles):
            xyzname = f'pairs_{"-".join(dimer_symbols)}-{i}.xyz'
            if i == n_xyzfiles - 1:
                i_distances = distances[i_split[i]:]
            else:
                i_distances = distances[i_split[i]:i_split[i + 1]]
            write_pair_xyz(i_distances, xyzname)
            xyz_out.append(xyzname)
        return xyz_out


def get_energies(elements, r_interval, N, quip_executable, quip_potfile, e_isolated,
                 ncores=1, datafile=None, xyzfile=None, spline_filtering=True):
    """ Main function to be called to calculate energies for all grid points. Input example:
    elements: one or many pairs, e.g. [A, B] or [[A, B], [B, B]]
              If one pair: ncores>1 parallelises over grid points.
              If many pairs, ncores has no effect and parallelisation is over all given pairs.
    r_interval: [0.01, 5]
    N: 1000
    quip_executable: '/path/to/quip'
    quip_potfile: 'gap.xml'
    e_isolated = {'A': -4.5, 'B': 0.0}
    ncores = 4
    datafile = 'AB.dat'
    spline_filtering: True  # must be done for tabgap potfile!!!
    """

    grid_points = generate_grid_points(*r_interval, N)

    # check if we have a list of pairs or just one pair
    if all(hasattr(t, '__iter__') and not isinstance(t, str) for t in elements):
        if not all(len(t) == 2 for t in elements):
            raise ValueError('bad pair list', elements)
        # multiple pairs, parallel over pairs (but not grid!)
        npairs = len(elements)
        xyz_in = []
        for pair in elements:
            xyz_in.extend(make_grid_points_xyz(grid_points, pair))
        parallel_over_pairs = True
    else:
        if len(elements) != 2:
            raise ValueError('bad pair', elements)
        # single pair , parallelise over grid
        xyz_in = make_grid_points_xyz(grid_points, elements, ncores)
        parallel_over_pairs = False
    xyz_out = 'temp.xyz'

    # run quip to get energies, with given parallelisation
    energies = run_quip_xyz_grid(quip_executable, quip_potfile, xyz_in, xyz_out, e_isolated,
                                 parallel_over_pairs)

    if datafile is not None:
        if parallel_over_pairs:
            print(' note: not writing energy data files for parallel_over_descriptors')
        else:
            write_datfile(grid_points, energies, datafile)

    # spline filtering required for tabgap pair_style, but not eam.fs
    if spline_filtering:
        if parallel_over_pairs:
            for i in range(npairs):
                energies[i] = filter_coeffs(*r_interval, [N], energies[i])
        else:
            energies = filter_coeffs(*r_interval, [N], energies)

    # clean up
    if xyzfile is not None:
        if npairs > 1:
            for i in range(npairs):
                os.rename(f'{xyz_out}-{i}', f'{xyzfile}-{i}')
        else:
            os.rename(xyz_out, xyzfile)
    dir = os.getcwd()
    files = os.listdir(dir)
    for file in files:
        if file.startswith('quip.out') or file.endswith('.idx') or file.startswith(xyz_out):
            os.remove(file)
    for xyz in xyz_in:
        os.remove(xyz)

    return energies
