import numpy as np
import time
import os
import shutil
import pathlib
import argparse
from datetime import datetime
from tabgap.quip_tools import GAP, make_zeroed_xml
from tabgap.write import write_tabgap_potfile, write_eam_fs_potfile
from tabgap.write import write_tabgap_potfile_h5
from tabgap.periodic_table import *
import tabgap.twobody
import tabgap.threebody
import tabgap.eam


class tabGAP:
    """ """

    def __init__(self, gap_xml, n2b=1000, n3b=(80, 80, 80), neam=1000,
                 r2b_min=0, r3b_min=0.1, rho_max=10, repcut_3b=(0, None),
                 elements=None, verbose=False):

        # parse GAP xml file
        gap = GAP(gap_xml)

        # check elements
        if elements is None:
            elements = gap.elements
        else:
            for e in elements:
                if e not in gap.elements:
                    raise ValueError(f'element {e} not found in GAP xml!')
        if len(elements) == 0:
            raise ValueError('No elements specified or found in GAP xml')

        # check descriptors
        for d in gap.nontab_descriptors:
            if d['name'] == 'distance_Nb':
                raise NotImplementedError('distance_Nb 2, 3 not yet supported, '
                                          'use distance_2b and angle_3b instead.')
        self.do_2b, self.do_3b, self.do_eam = False, False, False
        if len(gap.twobody) > 0:
            self.do_2b = True
        if len(gap.threebody) > 0:
            self.do_3b = True
        if len(gap.eam) > 0:
            self.do_eam = True

        # initialise grid parameters and exhaustive sanity-check..
        r2b = [r2b_min, gap.rcutmax_2b]
        r3b = [r3b_min, gap.rcutmax_3b]
        rhoeam = [0, rho_max]
        self.n2b, self.neam, self.n3b = None, None, None
        if self.do_3b:
            self.set_n3b(n3b)
            if r3b[0] > r3b[1] or r3b[0] < 0:
                raise ValueError(f'Bad 3b distance interval {r3b}')
            if (r3b[1] - r3b[0]) / min(n3b) > 0.1:
                print(f'!!!! WARNING!!!! : 3b grid n3b {n3b} is very sparse, make sure interpolation'
                      ' errors are sufficiently converged!\n')
        if self.do_2b and self.do_eam:
            if n2b != neam:
                raise ValueError('n2b and neam must be equal for EAM potential file!')
            # must use same range for pair pot. and pair density
            rcut_1D = max((gap.rcutmax_2b, gap.rcutmax_eam))
            r2b = [0, rcut_1D]  # eam file needs to start from r=0
        if self.do_2b:
            self.set_n2b(n2b)
            if r2b[0] > r2b[1]:
                raise ValueError(f'Bad 2b distance interval {r2b}')
            if (r2b[1] - r2b[0]) / n2b > 0.05:
                print(f'!!!! WARNING !!!!: 2b grid n2b {n2b} is very sparse, make sure interpolation'
                      ' errors are sufficiently converged!\n')
        if self.do_eam:
            self.set_neam(neam)
            self.set_rho_max(rho_max)
            if rho_max / neam > 0.05:
                print(f'!!!! WARNING !!!!: eam embedding grid neam {neam} is very sparse, make sure '
                      'interpolation errors are sufficiently converged!\n')

        # support multiple desciptors/embedding functions per element
        n_eams_element = []
        for element in elements:
            n_eams_element.append(len(gap.get_eam_descriptors([element])))
        n_eams = max(n_eams_element)

        xml = pathlib.Path(gap_xml).resolve()
        self.xmlname = str(xml.name)
        self.xmlpath = xml
        self.xmldir = xml.parent
        self.elements = elements
        self.gap = gap
        self.r2b = r2b
        self.r3b = r3b
        self.n_eams = n_eams
        self.repcut_3b = repcut_3b
        self.energies_2b = None
        self.energies_3b = None
        self.energies_eam = None
        self.energies_tabulated = False
        self.pair_style = None
        self.pair_coeff = None
        self.quip_command = None
        if verbose:
            self.print_info()


    def set_n2b(self, n2b):
        if n2b < 2:
            raise ValueError(f'Bad 2b grid n2b {n2b}, should be positive integer >> 0!')
        self.n2b = n2b
        if self.do_eam:
            self.neam = n2b  # must be equal for eam.fs
            self.energies_eam = None
        self.energies_2b = None
        self.energies_tabulated = False


    def set_neam(self, neam):
        if neam < 2:
            raise ValueError(f'Bad embdding energy grid neam {neam}, should be '
                             'positive integer >> 0!')
        self.neam = neam
        if self.do_2b:
            self.n2b = neam  # must be equal for eam.fs
            self.energies_2b = None
        self.energies_eam = None
        self.energies_tabulated = False


    def set_n3b(self, n3b):
        if len(n3b) != 3:
            raise ValueError(f'Bad 3b grid {n3b}, must be three integers')
        if n3b[0] != n3b[1]:
            raise ValueError(f'rij and rik in 3b grid n3b: (rij, rik, costheta) = {n3b}'
                             ' should be equal!')
        if min(n3b) < 2:
            raise ValueError(f'Bad 3b grid n3b {n3b}, should be positive integers >> 0!')
        if max(n3b) > 200:
            print(f'WARNING: 3b grid n3b {n3b} is very big, computing energy grid will '
                  'be very slow')
        self.n3b = n3b
        self.energies_3b = None
        self.energies_tabulated = False


    def set_rho_max(self, rho_max):
        if rho_max < 0:
            raise ValueError(f'Bad rho_max {rho_max}, must be positive')
        self.rho_max = rho_max
        self.energies_eam = None
        self.energies_tabulated = False


    def compute_energies(self, ncores=1, parallel_over_descriptors=False, datadir='out-tab',
                         save_data=False, rundir='out-quip', save_run=False,
                         save_xyz=False, verbose=False):
        """ main function to call when (re)computing energy grids """

        # check and set QUIP run command variable
        self.quip_command = os.environ.get('QUIP_COMMAND')
        if self.quip_command is None:
            raise OSError('Environment variable QUIP_COMMAND not set. '
                          'Do: export QUIP_COMMAND=/path/to/quip_executable')

        # make run directory and copy GAP potfiles there
        if os.path.exists(datadir):
            if verbose:
                print(f' WARNING: output folder {datadir} exists, some files may be overwritten')
        else:
            os.makedirs(datadir)
        if not os.path.exists(rundir):
            os.makedirs(rundir)
        for potfile in self.xmldir.glob(f'{self.xmlname}*'):
            shutil.copyfile(str(potfile), f'{rundir}/{str(potfile.name)}')
        # copy sparseX files explicitly, in case core_pot is GAP with different .xml name
        for potfile in self.xmldir.glob(f'*.sparseX.*'):
            shutil.copyfile(str(potfile), f'{rundir}/{str(potfile.name)}')

        if verbose:
            print(f' Using {ncores} CPU core(s)')

        time0 = time.time()

        # 2b
        if self.do_2b and self.energies_2b is None:
            time1 = time.time()
            self.compute_2b_energies(ncores, parallel_over_descriptors, datadir, rundir,
                                     save_run, save_xyz, verbose)
            print(f' compute 2b runtime: {np.round(time.time()-time1, 1)} s', flush=True)

        # 3b
        if self.do_3b and self.energies_3b is None:
            time2 = time.time()
            self.compute_3b_energies(ncores, parallel_over_descriptors, datadir,
                                     rundir, save_run, save_xyz, verbose)
            print(f' compute 3b runtime: {np.round(time.time()-time2, 1)} s', flush=True)

        # EAM embedding and pair density
        if self.do_eam and self.energies_eam is None:
            time3 = time.time()
            self.compute_eam_energies(ncores, datadir, rundir, save_run, save_xyz, verbose)
            print(f' compute EAM runtime: {np.round(time.time()-time3, 1)} s', flush=True)

        self.energies_tabulated = True

        if not save_run:
            shutil.rmtree(rundir)
        if not save_data:
            shutil.rmtree(datadir)
        if verbose:
            print(f'\ncompute_energies runtime: {np.round(time.time()-time0, 1)} s\n')


    def compute_2b_energies(self, ncores=1, parallel_over_descriptors=False, datadir='out-tab',
                            rundir='out-quip', save_run=False, save_xyz=False, verbose=False):
        """ pair potential energy grids """

        if verbose:
            print('\nComputing 2b energy grids...')

        # check out dirs
        if not os.path.exists(datadir):
            os.makedirs(datadir)
        if not os.path.exists(rundir):
            os.makedirs(rundir)
            for potfile in self.xmldir.glob(f'{self.xmlname}*'):
                shutil.copyfile(str(potfile), f'{rundir}/{str(potfile.name)}')

        time0 = time.time()
        energies_2b = {}

        # zero out 3b, eam GAPs to tabulate only pure 2b energies
        xml_2b = f'{rundir}/gap_2b_only.xml'
        make_zeroed_xml(self.xmlpath, xml_2b, zero_3b=True, zero_eam=True)

        # loop over pairs
        element_pairs = tabgap.twobody.get_element_pairs(self.elements)
        if not parallel_over_descriptors or ncores == 1:
            # ncores>1 parallelises over grid, one pair at a time
            for p, element_pair in enumerate(element_pairs):
                if verbose:
                    print(f' pair {p + 1}/{len(element_pairs)}: {element_pair}, '
                          f'timer: {np.round(time.time()-time0, 1)} s', flush=True)
                datfile = f"{datadir}/pair_energies_{''.join(element_pair)}.dat"
                if save_xyz:
                    xyzfile = f"{datadir}/pair_energies_{''.join(element_pair)}.xyz"
                else:
                    xyzfile = None
                # don't do spline filtering for eam potfile (last arg)
                energies_2b[element_pair] = tabgap.twobody.get_energies(element_pair,
                                            self.r2b, self.n2b, self.quip_command, xml_2b,
                                            self.gap.e0, ncores, datfile, xyzfile, not self.do_eam)
        else:
            # ncores>1 pairs at a time, with full grid for each pair/quip run
            if ncores > len(element_pairs):
                print(f' NOTE: requested more cores ({ncores}) than pairs'
                      f' ({len(element_pairs)}) for parallel_over_descriptors')
                chunks = [element_pairs]  # one chunk with all pairs
            else:
                chunks = [element_pairs[i:i+ncores] for i in range(0, len(element_pairs), ncores)]

            xyzfile = None
            if save_xyz:
                print(' NOTE: not saving .xyz files for parallel_over_descriptors')

            c = 0
            for chunk in chunks:
                if verbose:
                    for t in chunk:
                        print(f' pair {c + 1}/{len(element_pairs)}: {t}, '
                              f'timer: {np.round(time.time()-time0, 1)} s', flush=True)
                        c += 1
                _energies_2b_many = tabgap.twobody.get_energies(chunk,
                                               self.r2b, self.n2b, self.quip_command, xml_2b,
                                               self.gap.e0, 1, None, xyzfile, not self.do_eam)
                for i, t in enumerate(chunk):
                    energies_2b[t] = _energies_2b_many[i]

        self.energies_2b = energies_2b


    def compute_3b_energies(self, ncores=1, parallel_over_descriptors=False, datadir='out-tab',
                            rundir='out-quip', save_run=False, save_xyz=False, verbose=False):
        """ three-body potential energy grids """

        if verbose:
            print('\nComputing 3b energy grids...')

        # check out dirs
        if not os.path.exists(datadir):
            os.makedirs(datadir)
        if not os.path.exists(rundir):
            os.makedirs(rundir)
            for potfile in self.xmldir.glob(f'{self.xmlname}*'):
                shutil.copyfile(str(potfile), f'{rundir}/{str(potfile.name)}')

        time0 = time.time()
        energies_3b = {}

        # zero out 2b, eam GAPs and possible Glue potential to allow tabulating pure 3b energies
        xml_3b = f'{rundir}/gap_3b_only.xml'
        make_zeroed_xml(self.xmlpath, xml_3b, zero_2b=True, zero_eam=True)

        # loop over triplets
        element_triplets = tabgap.threebody.get_element_triplets(self.elements)
        if not parallel_over_descriptors or ncores == 1:
            # ncores>1 parallelises over grid, one triplet at a time
            for p, element_triplet in enumerate(element_triplets):
                if verbose:
                    print(f' triplet {p + 1}/{len(element_triplets)}: {element_triplet}, '
                          f'timer: {np.round(time.time()-time0, 1)} s', flush=True)
                datfile = f"{datadir}/triplet_energies_{''.join(element_triplet)}.dat"
                if save_xyz:
                    xyzfile = f"{datadir}/triplet_energies_{''.join(element_triplet)}.xyz"
                else:
                    xyzfile = None
                energies_3b[element_triplet] = tabgap.threebody.get_energies(element_triplet,
                                               self.r3b, self.n3b, self.quip_command, xml_3b,
                                               self.gap.e0, ncores, datfile, xyzfile, self.repcut_3b)
        else:
            # ncores>1 triplets at a time, with full grid for each triplet/quip run
            if ncores > len(element_triplets):
                print(f' NOTE: requested more cores ({ncores}) than triplets'
                      f' ({len(element_triplets)}) for parallel_over_descriptors')
                chunks = [element_triplets]  # one chunk with all triplets
            else:
                chunks = [element_triplets[i:i+ncores] for i in range(0, len(element_triplets), ncores)]

            xyzfile = None
            if save_xyz:
                print(' NOTE: not saving .xyz files for parallel_over_descriptors')

            c = 0
            for triplet_chunk in chunks:
                if verbose:
                    for t in triplet_chunk:
                        print(f' triplet {c + 1}/{len(element_triplets)}: {t}, '
                              f'timer: {np.round(time.time()-time0, 1)} s', flush=True)
                        c += 1
                _energies_3b_many = tabgap.threebody.get_energies(triplet_chunk,
                                               self.r3b, self.n3b, self.quip_command, xml_3b,
                                               self.gap.e0, 1, None, xyzfile, self.repcut_3b)
                for i, t in enumerate(triplet_chunk):
                    energies_3b[t] = _energies_3b_many[i]

        self.energies_3b = energies_3b


    def compute_eam_energies(self, ncores=1, datadir='out-tab', rundir='out-quip', save_run=False,
                             save_xyz=False, verbose=False):
        """ eam_density embedding energy and pair density grids """

        if verbose:
            print('\nComputing EAM energy grids...')

        # check out dirs
        if not os.path.exists(datadir):
            os.makedirs(datadir)
        if not os.path.exists(rundir):
            os.makedirs(rundir)
            for potfile in self.xmldir.glob(f'{self.xmlname}*'):
                shutil.copyfile(str(potfile), f'{rundir}/{str(potfile.name)}')

        time0 = time.time()

        energies_eam = []
        pair_densities = []
        for ii in range(self.n_eams):
            if verbose:
                print(f' EAM/embedding potential {ii+1}/{self.n_eams}')
            energies_eam.append({})
            pair_densities.append({})
            for jj, e1 in enumerate(self.elements):
                if verbose:
                    print(f'  element {jj+1}/{len(self.elements)}: {e1}, '
                          f'timer: {np.round(time.time()-time0, 1)} s', flush=True)
                do_zero = False
                try:
                    descriptor = self.gap.get_eam_descriptors([e1])[ii]
                except IndexError:
                    # exceeded number of descriptors for this element, zero everything
                    do_zero = True

                # pair densities
                for e2 in self.elements:
                    if do_zero:
                        # TODO write datfile?
                        pair_densities[ii][f'{e1}{e2}'] = np.zeros(self.n2b)
                    else:
                        datfile = f"{datadir}/pair_density_{e1}{e2}-{ii+1}.dat"
                        pair_densities[ii][f'{e1}{e2}'] = tabgap.eam.get_pair_densities((e1, e2),
                                                           self.r2b, self.n2b, descriptor, datfile)

                # embedding energies
                if do_zero:
                    energies_eam[ii][e1] = np.zeros(self.neam)
                else:
                    xml_eam = f'{rundir}/gap_eam_only-{e1}-{ii+1}.xml'
                    make_zeroed_xml(self.xmlpath, xml_eam, zero_2b=True, zero_3b=True,
                                    zero_eam=True, keep_these_eams=[descriptor['string']])
                    datfile = f"{datadir}/embedding_energies_{e1}-{ii+1}.dat"
                    if save_xyz:
                        xyzfile = f"{datadir}/embedding_energies_{e1}-{ii+1}.xyz"
                    else:
                        xyzfile = None
                    energies_eam[ii][e1] = tabgap.eam.get_energies((e1, e1), (0, self.rho_max),
                                                self.neam, descriptor, self.quip_command, xml_eam,
                                                self.gap.e0, ncores, datfile, xyzfile)
                    if ii == 0 and not self.do_3b:
                        energies_eam[ii][e1] += self.gap.e0[atomic_numbers[e1]]

        # needed for eam.fs potfile
        self.rij = tabgap.twobody.generate_grid_points(*self.r2b, self.n2b)
        rho_temp = tabgap.eam.generate_grid_points(0, self.rho_max, self.neam)
        self.dr = self.rij[-1] - self.rij[-2]
        self.drho = rho_temp[-1] - rho_temp[-2]

        self.energies_eam = energies_eam
        self.pair_densities = pair_densities


    def write_potential_files(self, prefix=None, verbose=False):
        if not self.energies_tabulated:
            raise Exception('energy grids not current or at all computed, run '
                            'tabGAP.compute_energies()')

        line1 = f"# DATE: {datetime.now()} UNITS: metal"
        if verbose:
            print('Potential files for LAMMPS:')

        # tabgap file
        tabgap_path = None
        if self.do_3b or (self.do_2b and not self.do_eam):
            line2 = f"# Tabulated GAP: {self.xmlname}, label: {self.gap.label} for pair_style tabgap"
            # TODO: print info about h5 and rawtext .tabgap files (use one, eventually only write h5?)
            if prefix is None:
                potfile_name = f"{'-'.join(sorted(self.elements))}.tabgap"
            else:
                potfile_name = f"{prefix}.tabgap"
            potfile_name_h5 = potfile_name + '.h5'
            if self.do_eam:
                # don't write 2b energies to tabgap
                write_tabgap_potfile(potfile_name, self.elements, self.gap.e0, {}, self.energies_3b,
                                     self.r2b, self.r3b, self.n2b, self.n3b, line1, line2)
                write_tabgap_potfile_h5(potfile_name_h5, self.elements, self.gap.e0, {}, self.energies_3b,
                                        self.r2b, self.r3b, self.n2b, self.n3b, line1, line2)
            else:
                write_tabgap_potfile(potfile_name, self.elements, self.gap.e0, self.energies_2b,
                                     self.energies_3b, self.r2b, self.r3b, self.n2b, self.n3b,
                                     line1, line2)
                write_tabgap_potfile_h5(potfile_name_h5, self.elements, self.gap.e0, self.energies_2b,
                                        self.energies_3b, self.r2b, self.r3b, self.n2b, self.n3b,
                                        line1, line2)
            if verbose:
                print(f'{potfile_name} or {potfile_name_h5}')
            tabgap_path = pathlib.Path(potfile_name).resolve()

        # eam.fs file(s)
        eam_paths = []
        if self.do_eam:
             # lattice constants and lattices not used in lammps
            lattices, lattice_constants = {}, {}
            for e in self.elements:
                lattices[e] = 'ZZZ'
                lattice_constants[e] = 1.0
            line2 = f"# Tabulated eam-GAP: {self.xmlname}, label: {self.gap.label}"
            line3 = "# pair_style eam/fs"
            if self.n_eams == 1:
                if prefix is None:
                    potfile_name = f"{'-'.join(sorted(self.elements))}.eam.fs"
                else:
                    potfile_name = f"{prefix}.eam.fs"
                write_eam_fs_potfile(potfile_name, self.elements, self.rij, self.energies_2b,
                                     self.pair_densities[0], self.energies_eam[0], self.dr, self.drho,
                                     self.r2b[1], lattice_constants, lattices, [line1, line2, line3])
                if verbose:
                    print(potfile_name)
                eam_paths.append(pathlib.Path(potfile_name).resolve())
            else:
                energies_2b_zeroed = self.energies_2b.copy()
                for key, value in energies_2b_zeroed.items():
                    energies_2b_zeroed[key] = np.zeros(len(value))
                for i in range(self.n_eams):
                    if prefix is None:
                        potfile_name = f"{'-'.join(sorted(self.elements))}-{i+1}.eam.fs"
                    else:
                        potfile_name = f"{prefix}-{i+1}.eam.fs"
                    if i == 0:
                        epair = self.energies_2b
                    else:
                        # zero 2b energies
                        epair = energies_2b_zeroed
                    write_eam_fs_potfile(potfile_name, self.elements, self.rij, epair,
                                         self.pair_densities[i], self.energies_eam[i], self.dr,
                                         self.drho, self.r2b[1], lattice_constants, lattices,
                                         [line1, line2, line3])
                    if verbose:
                        print(potfile_name)
                    eam_paths.append(pathlib.Path(potfile_name).resolve())

        self.set_lammps_pair_input(tabgap_path, eam_paths)
        if verbose:
            print('\nLAMMPS input (append elements to pair_coeff in correct order!):')
            if self.do_3b:
                print(' NOTE: Use .tabgap.h5 file for HDF5-compiled LAMMPS (recommended), otherwise .tabgap')
            print('pair_style ', self.pair_style)
            for pc in self.pair_coeff:
                print('pair_coeff ', pc)


    def set_lammps_pair_input(self, tabgap_path=None, eam_paths=None):
        """ Set pair_style and pair_coeff for lammps, given paths to potfiles as input.
        NOTE: element symbols must be appended to pair_coeff line(s) before running lammps!
        """

        if self.do_3b and self.do_eam:
            pair_style = 'hybrid/overlay tabgap'
            pair_coeff = [f'* * tabgap {tabgap_path}']
            for ef in eam_paths:
                pair_style += ' eam/fs'
                pair_coeff.append(f'* * eam/fs {ef}')
        elif self.do_3b:
            pair_style = 'tabgap'
            pair_coeff = [f'* * {tabgap_path}']
        elif self.do_eam:
            if self.n_eams == 1:
                pair_style = 'eam/fs'
                pair_coeff = [f'* * {eam_paths[0]}']
            else:
                pair_style = 'hybrid/overlay'
                pair_coeff = []
                for i, ef in enumerate(eam_paths):
                    pair_style += ' eam/fs'
                    pair_coeff.append(f'* * eam/fs {i+1} {ef}')
        elif self.do_2b:
            pair_style = 'tabgap'
            pair_coeff = [f'* * {tabgap_path}']
        self.pair_style = pair_style
        self.pair_coeff = pair_coeff


    def print_info(self):
        print(f' DATE: {datetime.now()}')
        print(f' Tabulating {self.gap.xmlfile} with GAP-label: {self.gap.label}')
        print('  Found elements:')
        print(f'   {self.gap.elements}')
        print('  Found non-zero e0:')
        for Z, e0 in enumerate(self.gap.e0):
            if abs(e0) > 1e-6:
                print(f'   {element_symbols[Z]} {e0}')
        print(f'  Found potentials: {self.gap.potentials}')
        print(f'  Found rcutmax_2b: {self.gap.rcutmax_2b}')
        print(f'  Found rcutmax_3b: {self.gap.rcutmax_3b}')
        print(f'  Found rcutmax_eam: {self.gap.rcutmax_eam}')
        print('  Found descriptors:')
        all_names = [d['name'] for d in self.gap.all_descriptors]
        for d in set(all_names):
            print(f'   {d}: {all_names.count(d)}')

        print(f'\n Tabulating interactions of elements {self.elements}:')
        if self.do_2b:
            print(f'  {len(tabgap.twobody.get_element_pairs(self.elements))} pair(s)')
            print(f'   2b grid: [rmin, rmax] = {self.r2b}, n = {self.n2b}')
        if self.do_3b:
            print(f'  {len(tabgap.threebody.get_element_triplets(self.elements))} triplet(s)')
            print(f'   3b grid: [rmin, rmax] = {self.r3b}, costheta -1 to 1, n = {self.n3b}')
        if self.do_eam:
            print(f'  {len(self.gap.eam)} embedding function(s)')
            print(f'   EAM grid: rho_max = {self.rho_max}, n = {self.neam}')
        print('', flush=True)


    def read_energy_grid(self):
        # read 2b, 3b, or eam grids from data file
        return


if __name__ == '__main__':
    default = argparse.ArgumentDefaultsHelpFormatter
    description = 'Tabulate 2b+3b(+EAM) GAP for lammps pair_style tabgap!'
    parser = argparse.ArgumentParser(description=description, formatter_class=default)
    parser.add_argument('xml', help='GAP main .xml file')
    parser.add_argument('-n2b', type=int, default=1000,
                        help='2b grid size (number of distances). Recommended: ~ 1000.')
    parser.add_argument('-n3b', nargs=3, type=int, default=[10, 10, 10],
                        help='3b grid size. Recommended: ~ 80 80 80.')
    parser.add_argument('-neam', type=int, default=1000,
                        help='eam density grid size (number of density points). Recommended: ~ 1000 '
                        '= same as 2b!')
    parser.add_argument('-r3bmin', type=float, default=0.1,
                        help='(optional) rmin for 3b grid.')
    parser.add_argument('-rhomax', type=float, default=10,
                        help='Maximum density for eam embedding energy grid [0, rhomax].')
    parser.add_argument('-e', nargs='+', type=str, default=None,
                        help='(optional) Specify elements to tabulate potentials for. '
                        'Tabulates all cross-interactions.'
                        ' By default, use all elements found in descriptors of GAP .xml file.')
    parser.add_argument('-nc', type=int, default=1,
                        help='Run this many quip jobs in parallel. Recommended > 1 to speed up'
                        ' 3b energy grid calculation!')
    parser.add_argument('-s', '--save', action='store_true', default=False,
                        help='Save created .xyz and .xml files.')
    parser.add_argument('-rep', metavar=('r1', 'r2'), nargs=2, type=float, default=[0, None],
                        help='EXPERIMENTAL! r1 and r2 for 3b reppot inner cutoff (E_3b = 0 for '
                        'r < r1), i.e. force 3b energies to zero in [r1, r2] with cutoff '
                        'function on rij and rik. '
                        'Reasonable values are so that r2-r1 >~ 0.5 for smooth and not-too-steep '
                        'cutoff, e.g. 0.2 0.8. Default is no reppot on 3b energies.')
    parser.add_argument('--pc', action='store_true', default=False,
                        help='Parallelise over descriptors instead of grid points (except EAM)')
    args = parser.parse_args()

    tg = tabGAP(args.xml, args.n2b, args.n3b, args.neam, r3b_min=args.r3bmin,
                rho_max=args.rhomax, repcut_3b=args.rep, elements=args.e, verbose=True)
    tg.compute_energies(args.nc, save_run=args.save, save_xyz=args.save, verbose=True,
                        parallel_over_descriptors=args.pc)
    tg.write_potential_files(verbose=True)
