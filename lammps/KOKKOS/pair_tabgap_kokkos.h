/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   https://www.lammps.org/, Sandia National Laboratories
   LAMMPS development team: developers@lammps.org

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */
//#include "Kokkos_Macros.hpp"
//#include "kokkos_type.h"
#ifdef PAIR_CLASS
// clang-format off
PairStyle(tabgap/kk,PairTabGAPKokkos<LMPDeviceType>);
PairStyle(tabgap/kk/device,PairTabGAPKokkos<LMPDeviceType>);
PairStyle(tabgap/kk/host,PairTabGAPKokkos<LMPHostType>);
// clang-format on
#else

// clang-format off
#ifndef LMP_PAIR_TABGAP_KOKKOS_H
#define LMP_PAIR_TABGAP_KOKKOS_H

#include "pair_tabgap.h"
#include "pair_kokkos.h"

// Tags for kernels
template<int NEIGHFLAG, int EVFLAG>
struct TagPairTabGAPCompute{};

struct TagPairTabGAPComputeShortNeigh{};

struct TagPairTabGAPInitNeighborTypes{};

struct TagPairTabGAPSortNeighbors{};



namespace LAMMPS_NS {

template<class DeviceType>
class PairTabGAPKokkos : public PairTabGAP {
 public:
  enum {EnabledNeighFlags=FULL};
  enum {COUL_FLAG=0};

  using TeamPolicySortN = Kokkos::TeamPolicy<DeviceType, TagPairTabGAPSortNeighbors>;
  using TeamMemberSortN = typename TeamPolicySortN::member_type;

  typedef DeviceType device_type;
  typedef ArrayTypes<DeviceType> AT;
  typedef EV_FLOAT value_type;

  PairTabGAPKokkos(class LAMMPS *);
  ~PairTabGAPKokkos() override;
  void compute(int, int) override;
  void coeff(int, char **) override;
  void init_style() override;

  // Compute and compute short neigh

  KOKKOS_INLINE_FUNCTION
  void operator()(TagPairTabGAPComputeShortNeigh, const int&) const;

  KOKKOS_INLINE_FUNCTION
  void operator()(TagPairTabGAPInitNeighborTypes, const int&) const;

  KOKKOS_INLINE_FUNCTION
  void operator()(TagPairTabGAPSortNeighbors, const TeamMemberSortN &t) const;

  template<int NEIGHFLAG, int EVFLAG>
  KOKKOS_INLINE_FUNCTION
  void operator()(TagPairTabGAPCompute<NEIGHFLAG, EVFLAG>, const int&) const;

  template<int NEIGHFLAG, int EVFLAG>
  KOKKOS_INLINE_FUNCTION
  void operator()(TagPairTabGAPCompute<NEIGHFLAG, EVFLAG>, const int&, EV_FLOAT&) const;

  KOKKOS_INLINE_FUNCTION
  void eval_cubic_splines_3d_kk(int, F_FLOAT, F_FLOAT, F_FLOAT, F_FLOAT*, F_FLOAT*) const;

  template<int NEIGHFLAG>
  KOKKOS_INLINE_FUNCTION
  void e_tally(EV_FLOAT &ev, const int &i, const int &j, const F_FLOAT &epair) const;

  template<int NEIGHFLAG>
  KOKKOS_INLINE_FUNCTION
  void v_tally3(EV_FLOAT &ev, const int &i, const int &j, const int &k,
                F_FLOAT *fj, F_FLOAT *fk, F_FLOAT *drij, F_FLOAT *drik) const;


 protected:
  typedef Kokkos::DualView<int*,Kokkos::LayoutRight,DeviceType> tdual_int_1d;
  typedef typename tdual_int_1d::t_dev_const_randomread t_int_1d_randomread;
  typedef typename tdual_int_1d::t_host t_host_int_1d;

  typedef Kokkos::DualView<int**,Kokkos::LayoutRight,DeviceType> tdual_int_2d;
  typedef typename tdual_int_2d::t_dev_const_randomread t_int_2d_randomread;
  typedef typename tdual_int_2d::t_host t_host_int_2d;

  typedef Kokkos::DualView<int***,Kokkos::LayoutRight,DeviceType> tdual_int_3d;
  typedef typename tdual_int_3d::t_dev_const_randomread t_int_3d_randomread;
  typedef typename tdual_int_3d::t_host t_host_int_3d;

  typedef Kokkos::DualView<LMP_FLOAT*,Kokkos::LayoutRight,DeviceType> tdual_float_1d;
  typedef typename tdual_float_1d::t_dev_const_randomread t_float_1d_randomread;
  typedef typename tdual_float_1d::t_host t_host_float_1d;

  typedef Kokkos::DualView<LMP_FLOAT**,Kokkos::LayoutRight,DeviceType> tdual_float_2d;
  typedef typename tdual_float_2d::t_dev_const_randomread t_float_2d_randomread;
  typedef typename tdual_float_2d::t_host t_host_float_2d;

  typename AT::t_x_array_randomread x;
  typename AT::t_f_array f;
  typename AT::t_tagint_1d tag;
  typename AT::t_int_1d_randomread type;

  DAT::tdual_efloat_1d k_eatom;
  DAT::tdual_virial_array k_vatom;
  typename AT::t_efloat_1d d_eatom;
  typename AT::t_virial_array d_vatom;

  int need_dup;

  using KKDeviceType = typename KKDevice<DeviceType>::value;

  template<typename DataType, typename Layout>
  using DupScatterView = KKScatterView<DataType, Layout, KKDeviceType, KKScatterSum, KKScatterDuplicated>;

  template<typename DataType, typename Layout>
  using NonDupScatterView = KKScatterView<DataType, Layout, KKDeviceType, KKScatterSum, KKScatterNonDuplicated>;

  DupScatterView<F_FLOAT*[3], typename DAT::t_f_array::array_layout> dup_f;
  DupScatterView<E_FLOAT*, typename DAT::t_efloat_1d::array_layout> dup_eatom;
  DupScatterView<F_FLOAT*[6], typename DAT::t_virial_array::array_layout> dup_vatom;

  NonDupScatterView<F_FLOAT*[3], typename DAT::t_f_array::array_layout> ndup_f;
  NonDupScatterView<E_FLOAT*, typename DAT::t_efloat_1d::array_layout> ndup_eatom;
  NonDupScatterView<F_FLOAT*[6], typename DAT::t_virial_array::array_layout> ndup_vatom;

  typename AT::t_neighbors_2d d_neighbors;
  typename AT::t_int_1d_randomread d_ilist;
  typename AT::t_int_1d_randomread d_numneigh;

  int neighflag,newton_pair;
  int nlocal,nall,eflag,vflag;

  int inum;
  Kokkos::View<int**,DeviceType> d_neighbors_short;
  Kokkos::View<int*,DeviceType> d_numneigh_short;

  Kokkos::View<LMP_FLOAT**,Kokkos::LayoutRight,DeviceType> d_Ad;
  Kokkos::View<LMP_FLOAT**,Kokkos::LayoutRight,DeviceType> d_dAd;
  Kokkos::View<LMP_FLOAT**,Kokkos::LayoutRight,DeviceType> d_d2Ad;
  Kokkos::View<LMP_FLOAT*,Kokkos::LayoutRight,DeviceType>  d_Bd;
  Kokkos::View<LMP_FLOAT*,Kokkos::LayoutRight,DeviceType>  d_Cd;
  Kokkos::View<LMP_FLOAT*,Kokkos::LayoutRight,DeviceType>  d_basis;

  Kokkos::View<int***,Kokkos::LayoutRight,DeviceType> d_map3b;
  Kokkos::View<LMP_FLOAT**,Kokkos::LayoutRight,DeviceType> d_lo_3body;
  Kokkos::View<LMP_FLOAT**,Kokkos::LayoutRight,DeviceType> d_hi_3body;
  Kokkos::View<int**,Kokkos::LayoutRight,DeviceType> d_grid_3body;
  Kokkos::View<LMP_FLOAT**,Kokkos::LayoutRight,DeviceType> d_fcoeff_3body;

  Kokkos::View<LMP_FLOAT*,Kokkos::LayoutRight,DeviceType> d_cut3bsq;
  Kokkos::View<LMP_FLOAT*,Kokkos::LayoutRight,DeviceType> d_e0;

  Kokkos::View<int*, DeviceType> d_neighbor_types;
  Kokkos::View<int*, DeviceType> d_neighbor_numbers;
  decltype(d_neighbors) d_neighbor_innertypes;

  friend void pair_virial_fdotr_compute<PairTabGAPKokkos>(PairTabGAPKokkos*);
};

}


#endif
#endif
