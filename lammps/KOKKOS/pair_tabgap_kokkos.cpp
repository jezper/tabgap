// clang-format off
/* -------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
-------------------------------------------------------------------- */

/* --------------------------------------------------------------------
   Contributing author: Jesper Byggmastar (Univ. of Helsinki)
   Based on MGP pair style from flare https://github.com/mir-group/flare by:
        Contributing authors: Lixin Sun (Harvard) Yu Xie (Harvard)
-------------------------------------------------------------------- */

#include "pair_tabgap_kokkos.h"

#include "Kokkos_Macros.hpp"
#include "Kokkos_ScatterView.hpp"
#include "atom_kokkos.h"
#include "atom_masks.h"
#include "comm.h"
#include "error.h"
#include "force.h"
#include "kokkos.h"
#include "math_const.h"
#include "kokkos_type.h"
#include "memory_kokkos.h"
#include "neigh_list_kokkos.h"
#include "neigh_request.h"
#include "neighbor.h"
#include "pair_kokkos.h"

#include <thrust/sort.h>
#include <thrust/execution_policy.h>

using namespace LAMMPS_NS;
using namespace MathConst;

template<class DeviceType>
PairTabGAPKokkos<DeviceType>::PairTabGAPKokkos(LAMMPS *lmp) : PairTabGAP(lmp) // check this
{
  respa_enable = 0;

  kokkosable = 1;
  atomKK = (AtomKokkos *) atom;
  execution_space = ExecutionSpaceFromDevice<DeviceType>::space;
  datamask_read = X_MASK | F_MASK | TAG_MASK | TYPE_MASK | ENERGY_MASK | VIRIAL_MASK;
  datamask_modify = F_MASK | ENERGY_MASK | VIRIAL_MASK;
}

template<class DeviceType>
PairTabGAPKokkos<DeviceType>::~PairTabGAPKokkos()
{
  if (!copymode) {
    memoryKK->destroy_kokkos(k_eatom,eatom);
    memoryKK->destroy_kokkos(k_vatom,vatom);
    eatom = nullptr;
    vatom = nullptr;
  }
}

template<class DeviceType>
void PairTabGAPKokkos<DeviceType>::compute(int eflag_in, int vflag_in)
{
  eflag = eflag_in;
  vflag = vflag_in;

  ev_init(eflag,vflag,0);

  //  reallocate per-atom arrays if necessary

  if (eflag_atom) {
    memoryKK->destroy_kokkos(k_eatom,eatom);
    memoryKK->create_kokkos(k_eatom,eatom,maxeatom,"pair:eatom");
    d_eatom = k_eatom.view<DeviceType>();
  }
  if (vflag_atom) {
    memoryKK->destroy_kokkos(k_vatom,vatom);
    memoryKK->create_kokkos(k_vatom,vatom,maxvatom,"pair:vatom");
    d_vatom = k_vatom.view<DeviceType>();
  }

  atomKK->sync(execution_space,datamask_read);
  if (eflag || vflag) atomKK->modified(execution_space,datamask_modify);
  else atomKK->modified(execution_space,F_MASK);

  x = atomKK->k_x.view<DeviceType>();
  f = atomKK->k_f.view<DeviceType>();
  tag = atomKK->k_tag.view<DeviceType>();
  type = atomKK->k_type.view<DeviceType>();
  nlocal = atom->nlocal;
  newton_pair = force->newton_pair;
  nall = atom->nlocal + atom->nghost;

  //const int inum = list->inum;
  inum = list->inum;
  const int ignum = inum + list->gnum;
  NeighListKokkos<DeviceType>* k_list = static_cast<NeighListKokkos<DeviceType>*>(list);
  d_ilist = k_list->d_ilist;
  d_numneigh = k_list->d_numneigh;
  d_neighbors = k_list->d_neighbors;

  need_dup = lmp->kokkos->need_dup<DeviceType>();
  if (need_dup) {
    dup_f = Kokkos::Experimental::create_scatter_view<Kokkos::Experimental::ScatterSum, Kokkos::Experimental::ScatterDuplicated>(f);
    dup_eatom = Kokkos::Experimental::create_scatter_view<Kokkos::Experimental::ScatterSum, Kokkos::Experimental::ScatterDuplicated>(d_eatom);
    dup_vatom = Kokkos::Experimental::create_scatter_view<Kokkos::Experimental::ScatterSum, Kokkos::Experimental::ScatterDuplicated>(d_vatom);
  } else {
    ndup_f = Kokkos::Experimental::create_scatter_view<Kokkos::Experimental::ScatterSum, Kokkos::Experimental::ScatterNonDuplicated>(f);
    ndup_eatom = Kokkos::Experimental::create_scatter_view<Kokkos::Experimental::ScatterSum, Kokkos::Experimental::ScatterNonDuplicated>(d_eatom);
    ndup_vatom = Kokkos::Experimental::create_scatter_view<Kokkos::Experimental::ScatterSum, Kokkos::Experimental::ScatterNonDuplicated>(d_vatom);
  }
  
  copymode = 1;
  
  // Neighbor list was rebuilt this timestep. Sort all lists by type
  // and create a sorted list of atom indices to loop over in compute.
  if (neighbor->ago == 0) {
    d_neighbor_types = decltype(d_neighbor_types)("d_neighbor_types", inum);
    d_neighbor_numbers = decltype(d_neighbor_numbers)("d_neighbor_numbers", inum);
    d_neighbor_innertypes = decltype(d_neighbor_innertypes)("d_neighbor_innertypes", inum, d_neighbors.extent(1));
    
    Kokkos::parallel_for(Kokkos::RangePolicy<DeviceType, TagPairTabGAPInitNeighborTypes>(0,inum), *this);
    const int teamsize = 128;
    const int numteams = (inum + teamsize - 1) / teamsize;
    Kokkos::parallel_for(TeamPolicySortN(numteams, teamsize), *this);
    
    constexpr auto thrust_policy = []() -> auto {
      if constexpr (std::is_same<DeviceType, LMPDeviceType>::value &&
		    !std::is_same<DeviceType, LMPHostType>::value) {
	return thrust::device;
      }
      else {
	return thrust::host;
      }
    }();

    thrust::sort_by_key(thrust_policy,
      Kokkos::Experimental::begin(d_neighbor_types),
      Kokkos::Experimental::begin(d_neighbor_types)+inum-1,
      Kokkos::Experimental::begin(d_neighbor_numbers));
  }

  EV_FLOAT ev;
  EV_FLOAT ev_all;
  
  // This is ragged in lammps, but not with Kokkos
  int max_neighs = d_neighbors.extent(1);
  
  // build short neighbor list
  
  if (((int) d_neighbors_short.extent(1) < max_neighs) ||
    ((int) d_neighbors_short.extent(0) < ignum)) {
    d_neighbors_short = Kokkos::View<int**,DeviceType>("TabGAP::neighbors_short",ignum*1.2,max_neighs);
  }
  if ((int)d_numneigh_short.extent(0) < ignum) {
    d_numneigh_short = Kokkos::View<int*,DeviceType>("TabGAP::numneighs_short",ignum*1.2);
  }
  
  Kokkos::parallel_for(Kokkos::RangePolicy<DeviceType,TagPairTabGAPComputeShortNeigh>(0,inum), *this);
 
  // loop over atoms
  // Full neighbor list is hardcoded. Run with HALFTHREAD for atomics.
  if (evflag)
    Kokkos::parallel_reduce(Kokkos::RangePolicy<DeviceType, TagPairTabGAPCompute<HALFTHREAD, 1>>(0,inum),*this,ev);
  else
    Kokkos::parallel_for(Kokkos::RangePolicy<DeviceType, TagPairTabGAPCompute<HALFTHREAD, 0>>(0,inum),*this);
  ev_all += ev;

  if (need_dup)
    Kokkos::Experimental::contribute(f, dup_f);
  
  if (eflag_global) eng_vdwl += ev_all.evdwl;
  if (vflag_global) {
    virial[0] += ev_all.v[0];
    virial[1] += ev_all.v[1];
    virial[2] += ev_all.v[2];
    virial[3] += ev_all.v[3];
    virial[4] += ev_all.v[4];
    virial[5] += ev_all.v[5];
  }

  if (eflag_atom) {
    if (need_dup)
      Kokkos::Experimental::contribute(d_eatom, dup_eatom);
    k_eatom.template modify<DeviceType>();
    k_eatom.template sync<LMPHostType>();
  }

  if (vflag_atom) {
    if (need_dup)
      Kokkos::Experimental::contribute(d_vatom, dup_vatom);
    k_vatom.template modify<DeviceType>();
    k_vatom.template sync<LMPHostType>();
  }

  if (vflag_fdotr) pair_virial_fdotr_compute(this);

  copymode = 0;

  // free duplicated memory
  if (need_dup) {
    dup_f            = decltype(dup_f)();
    dup_eatom        = decltype(dup_eatom)();
    dup_vatom        = decltype(dup_vatom)();
  }
}

template<class DeviceType>
KOKKOS_INLINE_FUNCTION
void PairTabGAPKokkos<DeviceType>::operator()(TagPairTabGAPComputeShortNeigh, const int &ii) const
{
  const int i = d_neighbor_numbers(ii);
  const int itype = type[i];
  const X_FLOAT xtmp = x(i,0);
  const X_FLOAT ytmp = x(i,1);
  const X_FLOAT ztmp = x(i,2);
  
  
  const int jnum = d_numneigh[i];
  int inside = 0;
  for (int jj = 0; jj < jnum; jj++) {
    int j = d_neighbors(i,jj);
    j &= NEIGHMASK;
    const int jtype = type[j];
    
    const X_FLOAT delx = x(j,0) - xtmp;
    const X_FLOAT dely = x(j,1) - ytmp;
    const X_FLOAT delz = x(j,2) - ztmp;
    F_FLOAT rsq = delx*delx + dely*dely + delz*delz;
    
    if (rsq < cutshortsq) {
      d_neighbors_short(ii,inside) = j;
      inside++;
    }
  }

  d_numneigh_short(ii) = inside;
}

template<class DeviceType>
KOKKOS_INLINE_FUNCTION
void PairTabGAPKokkos<DeviceType>::operator()(TagPairTabGAPInitNeighborTypes, const int &ii) const
{
  // Store types and indices of atoms and neighbors for sorting
  d_neighbor_types(ii) = type(ii);
  d_neighbor_numbers(ii) = ii;
  for (int j = 0; j < d_numneigh(ii); j++) {
    d_neighbor_innertypes(ii,j) = type(d_neighbors(ii,j));
  }
}

template<class DeviceType>
KOKKOS_INLINE_FUNCTION
void PairTabGAPKokkos<DeviceType>::operator()(TagPairTabGAPSortNeighbors, const TeamMemberSortN &t) const
{
  Kokkos::parallel_for(Kokkos::TeamThreadRange(t, t.team_size()), [=](const int) {
      const int thread_id = t.league_rank()*t.team_size()+t.team_rank();
      if (thread_id < inum) {
	auto ikeys = Kokkos::subview(d_neighbor_innertypes, thread_id, Kokkos::make_pair(0,d_numneigh(thread_id)));
	auto ineigh = Kokkos::subview(d_neighbors, thread_id, Kokkos::make_pair(0,d_numneigh(thread_id)));
	Kokkos::Experimental::sort_by_key_thread(t, ikeys, ineigh);
      }
    });
}


// Tabgap compute with 2body removed
template<class DeviceType>
template<int NEIGHFLAG, int EVFLAG>
KOKKOS_INLINE_FUNCTION
void PairTabGAPKokkos<DeviceType>::operator()(TagPairTabGAPCompute<NEIGHFLAG,EVFLAG>, const int &ii, EV_FLOAT &ev) const
{
  // The f array is duplicated for OpenMP, atomic for CUDA, and neither for Serial
  auto v_f = ScatterViewHelper<NeedDup_v<NEIGHFLAG,DeviceType>,decltype(dup_f),decltype(ndup_f)>::get(dup_f,ndup_f);
  auto a_f = v_f.template access<AtomicDup_v<NEIGHFLAG,DeviceType>>();
  
  const int i = d_neighbor_numbers(ii);//d_ilist[ii];
  const tagint itag = tag[i];
  const int itype = type[i];
  const X_FLOAT xtmp = x(i,0);
  const X_FLOAT ytmp = x(i,1);
  const X_FLOAT ztmp = x(i,2);
  
  F_FLOAT delr1[3];
  F_FLOAT delr2[3];
  F_FLOAT delr12[3];
  
  F_FLOAT evdwl = 0.0;
  
  const int jnum = d_numneigh_short[ii];
  
  // energy of isolated atom
  if (EVFLAG) {
    if (evflag) {
      ev.evdwl += d_e0[itype];
      this->template e_tally<NEIGHFLAG>(ev, i, i, d_e0[itype]);
    }
  }
  
  double fji[3] {0}, fki[3] {0};
  double fcosthetaijk_ij[3], fcosthetaijk_ik[3];
    
  // Might be clearer to loop to jnum-1
  for (int jj = 0; jj < jnum; jj++) {
    const int j = d_neighbors_short(ii,jj);
    const int jtype = type[j];
      
    delr1[0] = x(j,0) - xtmp;
    delr1[1] = x(j,1) - ytmp;
    delr1[2] = x(j,2) - ztmp;
    const F_FLOAT rsq1 = delr1[0] * delr1[0] + delr1[1] * delr1[1] + delr1[2] * delr1[2];

    for (int kk = jj + 1; kk < jnum; kk++) {
      const int k = d_neighbors_short(ii,kk);
      const int ktype = type[k];
      F_FLOAT ftriplet[3] = {0, 0, 0};
	
      int mapid1 = d_map3b(itype,jtype,ktype);
	
      F_FLOAT cutoff = d_cut3bsq(mapid1);
      if (rsq1 >= cutoff)
	continue;

      delr2[0] = x(k,0) - xtmp;
      delr2[1] = x(k,1) - ytmp;
      delr2[2] = x(k,2) - ztmp;
      F_FLOAT rsq2 =
	delr2[0] * delr2[0] + delr2[1] * delr2[1] + delr2[2] * delr2[2];
      if (rsq2 >= cutoff)
	continue;

      delr12[0] = x(k,0) - x(j,0);
      delr12[1] = x(k,1) - x(j,1);
      delr12[2] = x(k,2) - x(j,2);
      F_FLOAT rsq12 = delr12[0] * delr12[0] + delr12[1] * delr12[1] +
	delr12[2] * delr12[2];

      // TODO flag jk_cutoff[i][j][k] ?
      //if (rsq12 >= cutoff)
      //continue;

      // compute bonds
      F_FLOAT rij = Kokkos::sqrt(rsq1);
      F_FLOAT rik = Kokkos::sqrt(rsq2);
      // rjk = sqrt(rsq12);

      // compute angle
      F_FLOAT cos_angle = (rsq1+rsq2-rsq12)/2./(rij*rik);
      if (cos_angle > 1) cos_angle = 1; // prevent numerical error
      if (cos_angle < -1) cos_angle = -1;

      // compute spline
      eval_cubic_splines_3d_kk(mapid1, rij, rik, cos_angle, &evdwl, ftriplet);
      
      double f_ij, f_ik;
      
      f_ij = ftriplet[0] / rij;
      f_ik = ftriplet[1] / rik;

      // using fji = -fij to get correct sign for input to virial calculation ev_tally*
      fji[0] = -f_ij * delr1[0]; // delr1, delr2, not unit vector
      fji[1] = -f_ij * delr1[1];
      fji[2] = -f_ij * delr1[2];
      fki[0] = -f_ik * delr2[0];
      fki[1] = -f_ik * delr2[1];
      fki[2] = -f_ik * delr2[2];
      fcosthetaijk_ij[0] = ftriplet[2] / rij * (delr2[0] / rik - delr1[0] / rij * cos_angle);
      fcosthetaijk_ij[1] = ftriplet[2] / rij * (delr2[1] / rik - delr1[1] / rij * cos_angle);
      fcosthetaijk_ij[2] = ftriplet[2] / rij * (delr2[2] / rik - delr1[2] / rij * cos_angle);
      fcosthetaijk_ik[0] = ftriplet[2] / rik * (delr1[0] / rij - delr2[0] / rik * cos_angle);
      fcosthetaijk_ik[1] = ftriplet[2] / rik * (delr1[1] / rij - delr2[1] / rik * cos_angle);
      fcosthetaijk_ik[2] = ftriplet[2] / rik * (delr1[2] / rij - delr2[2] / rik * cos_angle);

      // final forces on j and k due to i. force on i is now: -fji - fki
      fji[0] -= fcosthetaijk_ij[0];
      fji[1] -= fcosthetaijk_ij[1];
      fji[2] -= fcosthetaijk_ij[2];
      fki[0] -= fcosthetaijk_ik[0];
      fki[1] -= fcosthetaijk_ik[1];
      fki[2] -= fcosthetaijk_ik[2];
      
      a_f(i,0) += (-fji[0] - fki[0]);
      a_f(i,1) += (-fji[1] - fki[1]);
      a_f(i,2) += (-fji[2] - fki[2]);
      a_f(j,0) += fji[0];
      a_f(j,1) += fji[1];
      a_f(j,2) += fji[2];
      a_f(k,0) += fki[0];
      a_f(k,1) += fki[1];
      a_f(k,2) += fki[2];

      if (EVFLAG) {
	if (evflag) {
	  // tally full energy of i
	  ev.evdwl += evdwl;
	  this->template e_tally<NEIGHFLAG>(ev, i, i, evdwl);
	}
	if (vflag_either) {
	  this->template v_tally3<NEIGHFLAG>(ev, i, j, k, fji, fki, delr1, delr2);
	}
      }
    } // k
  } // j
}

template<class DeviceType>
template<int NEIGHFLAG, int EVFLAG>
KOKKOS_INLINE_FUNCTION
void PairTabGAPKokkos<DeviceType>::operator()(TagPairTabGAPCompute<NEIGHFLAG, EVFLAG>, const int &ii) const {
  EV_FLOAT ev;
  this->template operator()<NEIGHFLAG,EVFLAG>(TagPairTabGAPCompute<NEIGHFLAG, EVFLAG>(), ii, ev);
}

template <class DeviceType>
KOKKOS_INLINE_FUNCTION
void PairTabGAPKokkos<DeviceType>::eval_cubic_splines_3d_kk(int mapid, F_FLOAT r1, F_FLOAT r2,
                                    F_FLOAT a12, F_FLOAT *val, F_FLOAT *dval) const {
  auto a = Kokkos::subview(d_lo_3body, mapid, Kokkos::ALL);
  auto b = Kokkos::subview(d_hi_3body, mapid, Kokkos::ALL);
  auto orders = Kokkos::subview(d_grid_3body, mapid, Kokkos::ALL);
  auto coefs = Kokkos::subview(d_fcoeff_3body, mapid, Kokkos::ALL);
  const int dim = 3;
  int i;
  int j;
  F_FLOAT point[3] = {r1, r2, a12};
  F_FLOAT dinv[dim];
  F_FLOAT u[dim];
  F_FLOAT i0[dim];
  int ii[dim];
  F_FLOAT tt[dim];
  
  *val = 0;
  // coefficients
  for (i = 0; i < dim; i++) {
    dinv[i] = (orders(i) - 1.0) / (b(i) - a(i));
    u[i] = (point[i] - a(i)) * dinv[i];
    i0[i] = Kokkos::floor(u[i]);
    ii[i] = Kokkos::fmax(Kokkos::fmin(i0[i], orders(i) - 2), 0);
    tt[i] = u[i] - ii[i];
  }

  // points
  F_FLOAT tp[dim][4];
  for (i = 0; i < dim; i++) {
    tp[i][3] = 1.0;
    tp[i][2] = tt[i];
    tp[i][1] = tt[i]*tt[i];
    tp[i][0] = tp[i][2]*tp[i][1];
  }

  F_FLOAT Phi[dim][4], dPhi[dim][4];
  F_FLOAT dt;
  int k;

  for (j = 0; j < dim; j++) {

    // evaluate spline function
    if (tt[j] < 0) {
      for (i = 0; i < 4; i++) {
        Phi[j][i] = d_dAd(i,3) * tt[j] + d_Ad(i,3);
      }
    } else if (tt[j] > 1) {
      dt = tt[j] - 1;
      for (i = 0; i < 4; i++) {
        Phi[j][i] = d_Bd(i) * dt + d_Cd(i);
      }
    } else {
      for (i = 0; i < 4; i++) {
        Phi[j][i] = 0;
        for (k = 0; k < 4; k++) {
          Phi[j][i] += d_Ad(i,k) * tp[j][k];
        }
      }
    }

    // evaluate derivatives
    for (i = 0; i < 4; i++) {
      dPhi[j][i] = 0;
      for (k = 0; k < 4; k++) {
        dPhi[j][i] += d_dAd(i,k) * tp[j][k];
      }
      dPhi[j][i] *= dinv[j];
    }
  }

  // added by coefficients
  int N[dim];
  for (i = 0; i < dim; i++) {
    N[i] = orders(i) + 2;
  }

  for (i = 0; i < 4; i++) {
    F_FLOAT ppc = 0;
    F_FLOAT dppc1 = 0;
    F_FLOAT dppc2 = 0;
    for (j = 0; j < 4; j++) {
      F_FLOAT pc = 0;
      F_FLOAT dpc = 0;
      for (k = 0; k < 4; k++) {
        F_FLOAT c = coefs(((ii[0] + i) * N[1] + ii[1] + j) * N[2] + ii[2] + k);
        pc += Phi[2][k] * c;
        dpc += dPhi[2][k] * c;
      }
      ppc += Phi[1][j] * pc;
      dppc1 += dPhi[1][j] * pc;
      dppc2 += Phi[1][j] * dpc;
    }
    *val += Phi[0][i] * ppc;
    dval[0] += dPhi[0][i] * ppc;
    dval[1] += Phi[0][i] * dppc1;
    dval[2] += Phi[0][i] * dppc2;
  }
}

template<class DeviceType>
template<int NEIGHFLAG>
KOKKOS_INLINE_FUNCTION
void PairTabGAPKokkos<DeviceType>::e_tally(EV_FLOAT &ev, const int &i, const int &j, const F_FLOAT &epair) const {
  auto v_eatom = ScatterViewHelper<NeedDup_v<NEIGHFLAG,DeviceType>,decltype(dup_eatom),decltype(ndup_eatom)>::get(dup_eatom,ndup_eatom);
  auto a_eatom = v_eatom.template access<AtomicDup_v<NEIGHFLAG,DeviceType>>();
  if (eflag_atom) {
    const E_FLOAT epairhalf = 0.5 * epair;
    a_eatom[i] += epairhalf;
    a_eatom[j] += epairhalf;
  }
}

template<class DeviceType>
template<int NEIGHFLAG>
KOKKOS_INLINE_FUNCTION
void PairTabGAPKokkos<DeviceType>::v_tally3(EV_FLOAT &ev,
        const int &i, const int &j, const int &k,
        F_FLOAT *fj, F_FLOAT *fk, F_FLOAT *drij, F_FLOAT *drik) const
{
  // The vatom array is duplicated for OpenMP, atomic for CUDA, and neither for Serial

  auto v_vatom = ScatterViewHelper<NeedDup_v<NEIGHFLAG,DeviceType>,decltype(dup_vatom),decltype(ndup_vatom)>::get(dup_vatom,ndup_vatom);
  auto a_vatom = v_vatom.template access<AtomicDup_v<NEIGHFLAG,DeviceType>>();

  F_FLOAT v[6];

  v[0] = (drij[0]*fj[0] + drik[0]*fk[0]);
  v[1] = (drij[1]*fj[1] + drik[1]*fk[1]);
  v[2] = (drij[2]*fj[2] + drik[2]*fk[2]);
  v[3] = (drij[0]*fj[1] + drik[0]*fk[1]);
  v[4] = (drij[0]*fj[2] + drik[0]*fk[2]);
  v[5] = (drij[1]*fj[2] + drik[1]*fk[2]);

  if (vflag_global) {
    ev.v[0] += v[0];
    ev.v[1] += v[1];
    ev.v[2] += v[2];
    ev.v[3] += v[3];
    ev.v[4] += v[4];
    ev.v[5] += v[5];
  }

  if (vflag_atom) {
    v[0] *= THIRD;
    v[1] *= THIRD;
    v[2] *= THIRD;
    v[3] *= THIRD;
    v[4] *= THIRD;
    v[5] *= THIRD;

    a_vatom(i,0) += v[0]; a_vatom(i,1) += v[1]; a_vatom(i,2) += v[2];
    a_vatom(i,3) += v[3]; a_vatom(i,4) += v[4]; a_vatom(i,5) += v[5];

    a_vatom(j,0) += v[0]; a_vatom(j,1) += v[1]; a_vatom(j,2) += v[2];
    a_vatom(j,3) += v[3]; a_vatom(j,4) += v[4]; a_vatom(j,5) += v[5];

    a_vatom(k,0) += v[0]; a_vatom(k,1) += v[1]; a_vatom(k,2) += v[2];
    a_vatom(k,3) += v[3]; a_vatom(k,4) += v[4]; a_vatom(k,5) += v[5];
  }
}

template<class DeviceType>
void PairTabGAPKokkos<DeviceType>::coeff(int narg, char **arg)
{
  PairTabGAP::coeff(narg, arg);
  
  int ntype_p1 = atom->ntypes + 1;

  tdual_int_3d k_map3b("k_map3b", ntype_p1, ntype_p1, ntype_p1);
  t_host_int_3d h_map3b = k_map3b.h_view;
  
  tdual_float_1d k_e0("k_e0", ntype_p1);
  t_host_float_1d h_e0 = k_e0.h_view;
  for (int i = 1; i < ntype_p1; i++) {
    for (int j = 1; j < ntype_p1; j++) {
      for (int k = 1; k < ntype_p1; k++) {
	h_map3b(i,j,k) = map3b[i][j][k];
      }
    }
    h_e0(i) = e0[i];
  }
  k_map3b.template modify<LMPHostType>();
  k_map3b.template sync<DeviceType>();
  d_map3b = k_map3b.template view<DeviceType>();
  
  k_e0.template modify<LMPHostType>();
  k_e0.template sync<DeviceType>();
  d_e0 = k_e0.template view<DeviceType>();
  
  tdual_float_2d k_lo_3body("k_lo_3body", n_3body, 3);
  t_host_float_2d h_lo_3body = k_lo_3body.h_view;
  
  tdual_float_2d k_hi_3body("k_hi_3body", n_3body, 3);
  t_host_float_2d h_hi_3body = k_hi_3body.h_view;

  tdual_int_2d k_grid_3body("k_grid_3body", n_3body, 3);
  t_host_int_2d h_grid_3body = k_grid_3body.h_view;
  
  tdual_float_1d k_cut3bsq("k_cut3bsq", n_3body);
  t_host_float_1d h_cut3bsq = k_cut3bsq.h_view;
  
  for (int i = 0; i < n_3body; i++) {
    for (int j = 0; j < 3; j++) {
      h_lo_3body(i,j) = lo_3body[i][j];
      h_hi_3body(i,j) = hi_3body[i][j];
      h_grid_3body(i,j) = grid_3body[i][j];
    }
    h_cut3bsq(i) = cut3bsq[i];
  }
  k_lo_3body.template modify<LMPHostType>();
  k_hi_3body.template modify<LMPHostType>();
  k_grid_3body.template modify<LMPHostType>();
  k_cut3bsq.template modify<LMPHostType>();
  k_lo_3body.template sync<DeviceType>();
  k_hi_3body.template sync<DeviceType>();
  k_grid_3body.template sync<DeviceType>();
  k_cut3bsq.template sync<DeviceType>();
  d_lo_3body = k_lo_3body.template view<DeviceType>();
  d_hi_3body = k_hi_3body.template view<DeviceType>();
  d_grid_3body = k_grid_3body.template view<DeviceType>();
  d_cut3bsq = k_cut3bsq.template view<DeviceType>();
  
  int maxlen = 0;
  for (int i = 0; i < n_3body; i++) {
    int len = (grid_3body[i][0] + 2)
      * (grid_3body[i][1] + 2)
      * (grid_3body[i][2] + 2);
    maxlen = std::max(maxlen, len);
  }
  tdual_float_2d k_fcoeff_3body("k_fcoeff3body", n_3body, maxlen);
  t_host_float_2d h_fcoeff_3body = k_fcoeff_3body.h_view;
  for (int i = 0; i < n_3body; i++) {
    int len = (grid_3body[i][0] + 2)
      * (grid_3body[i][1] + 2)
      * (grid_3body[i][2] + 2);
    for (int j = 0; j < len; j++) {
      h_fcoeff_3body(i,j) = fcoeff_3body[i][j];
    }
  }
  k_fcoeff_3body.template modify<LMPHostType>();
  k_fcoeff_3body.template sync<DeviceType>();
  d_fcoeff_3body = k_fcoeff_3body.template view<DeviceType>();
  
  tdual_float_2d k_Ad("k_Ad", 4, 4),
    k_dAd("k_dAd", 4, 4),
    k_d2Ad("k_d2Ad", 4,4);
  tdual_float_1d k_Bd("k_Bd", 4),
    k_Cd("k_Cd", 4);
  t_host_float_2d h_Ad = k_Ad.h_view;
  t_host_float_2d h_dAd = k_dAd.h_view;
  t_host_float_2d h_d2Ad = k_d2Ad.h_view;
  t_host_float_1d h_Bd = k_Bd.h_view;
  t_host_float_1d h_Cd = k_Cd.h_view;
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      h_Ad(i,j) = Ad[i][j];
      h_dAd(i,j) = dAd[i][j];
      h_d2Ad(i,j) = d2Ad[i][j];
    }
    h_Bd(i) = Bd[i];
    h_Cd(i) = Cd[i];
  }
  
  k_Ad.template modify<LMPHostType>();
  k_Ad.template sync<DeviceType>();
  d_Ad = k_Ad.template view<DeviceType>();
  
  k_dAd.template modify<LMPHostType>();
  k_dAd.template sync<DeviceType>();
  d_dAd = k_dAd.template view<DeviceType>();

  k_d2Ad.template modify<LMPHostType>();
  k_d2Ad.template sync<DeviceType>();
  d_d2Ad = k_d2Ad.template view<DeviceType>();

  k_Bd.template modify<LMPHostType>();
  k_Bd.template sync<DeviceType>();
  d_Bd = k_Bd.template view<DeviceType>();

  k_Cd.template modify<LMPHostType>();
  k_Cd.template sync<DeviceType>();
  d_Cd = k_Cd.template view<DeviceType>();
}

template<class DeviceType>
void PairTabGAPKokkos<DeviceType>::init_style()
{
  PairTabGAP::init_style();

  // adjust neighbor list request for KOKKOS
  neighflag = lmp->kokkos->neighflag;
  auto request = neighbor->find_request(this);
  request->set_kokkos_host(std::is_same<DeviceType,LMPHostType>::value &&
                           !std::is_same<DeviceType,LMPDeviceType>::value);
  request->set_kokkos_device(std::is_same<DeviceType,LMPDeviceType>::value);
}

namespace LAMMPS_NS {
template class PairTabGAPKokkos<LMPDeviceType>;
#ifdef LMP_KOKKOS_GPU
template class PairTabGAPKokkos<LMPHostType>;
#endif
}
